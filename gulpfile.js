"use strict";
//Turn on production or development
process.env.NODE_ENV = 'production';

//Plugins to include
var
    gulp       = require('gulp'),
    newer      = require('gulp-newer'),
    concat     = require('gulp-concat'),
    preprocess = require('gulp-preprocess'),
    htmlClean  = require('gulp-htmlclean'),
    pleeease   = require('gulp-pleeease'),
    jshint     = require('gulp-jshint'),
    deporder   = require('gulp-deporder'),
    sass       = require('gulp-sass'),
    stripdebug = require('gulp-strip-debug'),
    uglify     = require('gulp-uglify'),
    size       = require('gulp-size'),
    del        = require('del'),
    imagemin = require('gulp-imagemin'),
    browsersync = require('browser-sync'),
    pkg        = require('./package.json')
    //browserify = require('gulp-browserify')

    ;

//File locations
var devBuild = ((process.env.NODE_ENV || 'development').trim().toLowerCase() !== 'production'),
    source   = 'source/',
    dest     = 'build/',
    html     = {
        in     : source + '*.html',
        watch  : [source + '*.html', source + 'template/**/*'],
        out    : dest,
        context: {
            devBuild: devBuild,
            author  : pkg.author,
            version : pkg.version
        }
    },
    images = {
        in : source + 'images/*.*',
        out: dest + 'images/'
    },
    css      = {
        in          : source + 'scss/style.scss',
        watch       : [source + 'scss/**/*'],
        out         : dest + 'css/',
        sassOpts    : {
            outputStyle    : 'nested',
            imagePath      : '../images',
            prcision       : 3,
            errLogToConsole: true
        },
        pleeeaseOpts: {
            autoprefixer  : {
                browsers: ["last 2 versions", "> 2%"]
            },
            rem           : ['16px'],
            psuedoElements: true,
            mpacker       : true,
            minifier      : !devBuild
        }
    },
    js       = {
        in      : source + 'js/**/*',
        out     : dest + 'js/',
        filename: 'main.js'
    },
    syncOpts = {
        server: {
            baseDir: dest,
            index: 'index.html'
        },
        open: false,
        notify: true
    };

console.log(pkg.name + ' ' + pkg.version + ', ' + (devBuild ? 'development' : 'production') + ' build');
//Clean the build folder
gulp.task('clean', function () {
    del([dest + '*']);
});

//build html files
gulp.task('html', function () {
    var page = gulp.src(html.in).pipe(preprocess({context: html.context}));
    if (!devBuild) {
        page = page.pipe(size({title: 'HTML in'}))
            .pipe(htmlClean())
            .pipe(size({title: 'HTML out'}))
        ;
    }
    return page.pipe(gulp.dest(html.out))
});

//CSS tasks
gulp.task('sass', function () {
    return gulp.src(css.in)
        .pipe(sass(css.sassOpts))
        .pipe(size({title: "CSS in "}))
        .pipe(pleeease(css.pleeeaseOpts))
        .pipe(size({title: "CSS out "}))
        .pipe(gulp.dest(css.out))
        .pipe(browsersync.reload({stream: true}))
});

//JS tasks
gulp.task("js", function () {
    if (devBuild === "development") {
        del([dest + 'js/*']);
        return gulp.src(js.in)
            .pipe(newer(js.out))
            .pipe(jshint())
            .pipe(jshint.reporter('default'))
            .pipe(jshint.reporter('fail'))
            .pipe(gulp.dest(js.out))
    }
    else {
        del([dest + 'js/*']);
        return gulp.src(js.in)
            .pipe(deporder())
            //.pipe(browserify())
            .pipe(concat(js.filename))
            .pipe(size({title: "JS in "}))
            .pipe(stripdebug())
            .pipe(uglify())
            .pipe(size({title: "JS Out "}))
            .pipe(gulp.dest(js.out))
    }
});
//images
gulp.task('images', function () {
    return gulp.src(images.in)
        .pipe(newer(images.out))
        .pipe(imagemin())
        .pipe(gulp.dest(images.out))
});
//Broswer sync
gulp.task('browsersync', function () {
    browsersync(syncOpts);
});
//Default task
gulp.task('default', ['html', 'sass', 'js', 'browsersync', 'images'], function () {
    gulp.watch(html.watch, ['html', browsersync.reload]);
    gulp.watch(css.in, ['sass']);
    gulp.watch(js.in, ['js', browsersync.reload]);
    gulp.watch(images.in, ['images']);
});