/* requires:
material.js
jquery.js
ko.js
*/
var map;
function renderMap(data,zoom) {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom     : zoom,
        center   : data.center,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
}
//Create globals to set up markers and info bubble
var globals = {
    infoDiv: $('.js-info-wrapper'),
    icon: 'http://www.jonochoa.com/projects/nanodegree/project05/images/marker-click.png',
    iconClick: 'http://www.jonochoa.com/projects/nanodegree/project05/images/marker.png',
    infoWindow: null,
    markers: [
        {
            name   : 'Buckingham Palace',
            center : {
                lat: 51.501364,
                lng: -0.14189
            }
        },
        {
            name   : 'Westminster Abbey',
            center : {
                lat: 51.4991836,
                lng: -0.1274721
            }
        },
        {
            name   : 'Tower of London',
            center : {
                lat: 51.5081124,
                lng: -0.0759493
            }
        },
        {
            name   : 'Tower Bridge',
            center : {
                lat: 51.5054564,
                lng: -0.0753565
            }
        },
        {
            name   : 'London Eye',
            center : {
                lat: 51.503324,
                lng: -0.119543
            }
        },
        {
            name   : "Big Ben",
            center : {
                lat: 51.5007292,
                lng: -0.1246254
            }
        },
        {
            name   : "Churchill War Rooms",
            center : {
                lat: 51.5022184,
                lng: -0.129205
            }
        },
        {
            name   : "London Bridge",
            center : {
                lat: 51.5078788,
                lng: -0.0877321
            }
        },
        {
            name   : "House of Commons",
            center : {
                lat: 51.4997937,
                lng: -0.1246931
            }
        },
        {
            name   : "Shakespeare's Globe",
            center : {
                lat: 51.508076,
                lng: -0.097194
            }
        }
    ]
};

//Marker function to initialize all markers
function Marker(data) {
    var self = this;
    self.infowindow = null;
    self.name = ko.observable(data.name);
    self.center = data.center;
    self.content = ko.observable();
    self.link = ko.observable();
    self.isVisible = ko.observable();

    self.marker = new google.maps.Marker({
        position : new google.maps.LatLng(data.center.lat, data.center.lng),
        title    : data.name,
        map      : map,
        draggable: false,
        icon: globals.icon,
        animation: google.maps.Animation.DROP
    });
    self.isVisible = ko.observable(false);
    self.isVisible.subscribe(function (currentState) {
        if (currentState) {
            self.marker.setMap(map);
        } else {
            self.marker.setMap(null);
        }
    });
    self.isVisible(true);

    self.getInfo = function () {
        var wikiName = encodeURI(data.name);
        var wikiUrl = "http://en.wikipedia.org/w/api.php?&action=opensearch&search=" + wikiName + "&format=json&callback=?";
        $.ajax({
            url     : wikiUrl,
            dataType: 'jsonp',
            success : function (data) {
                self.content(data[2][0]);
                self.link(data[3][0]);
                var content = '<div class="str-info-window"><div class="demo-card-wide mdl-card mdl-shadow--2dp"><div class="mdl-card__title"><h2 class="mdl-card__title-text js-title">' +
                    self.name() +
                    '</h2></div><div class="mdl-card__supporting-text js-content">' + self.content() +
                    '</div><div class="mdl-card__actions mdl-card--border">' +
                    '<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect js-link" href="' + self.link() + '" target="_blank">' +
                    'Read More (Wikipedia)</a></div></div>';

                globals.infoWindow.setContent(content);
            },
            error: function () {
                self.content("Sorry, I was unable to fetch the data.");
            }
        });
    };

    self.openInfoWindow = function () {
        globals.infoWindow.setContent("<div class='mdl-spinner mdl-js-spinner is-active'></div>");
        self.getInfo();
        globals.infoWindow.open(map, self.marker);
        self.marker.setAnimation(google.maps.Animation.BOUNCE);
        setTimeout(function () {
            self.marker.setAnimation(null);
        }, 2000);
        map.setCenter(self.center);
        map.setZoom(18);
    };
    google.maps.event.addListener(self.marker, 'click', self.openInfoWindow);
}

//Knockout view model
var viewModel = function () {
    var self = this;
    //Placeholder for current marker
    self.currentMarker = ko.observable();
    self.markerList = ko.observableArray();
    globals.markers.forEach(function (data) {
        self.markerList.push(new Marker(data));
    });
    //Create all markers based off of the markers in globals
    self.resetMarkers = function () {
        self.markerList = ko.observableArray();
        globals.markers.forEach(function (data) {
            self.markerList.push(new Marker(data));
        });

    };

    //Renders the map on the page
    self.renderMap = function (data, zoom) {
        $('#map').empty();
        renderMap(data,zoom);
    };
    self.markerClick = function (marker) {
        marker.openInfoWindow();
        self.currentMarker(marker);
    };

    //Filter observable for filtering list items
    self.filter = ko.observable("");
    //Filter function which filters out items in the list when you start typing.
    self.filteredItems = ko.computed(function () {
        var filter = self.filter().toLowerCase();
        if (!filter) {
            return self.markerList();
        } else {
            return ko.utils.arrayFilter(self.markerList(), function (marker) {
                var item = (marker.name().toLowerCase().indexOf(filter) !== -1);
                marker.isVisible(item);
                return item;
            });
        }
    }, self);
    self.errorAlert = ko.observable();
    //Used to reset the map
    self.resetMap = function () {
        if(globals.markers.length) {
            self.renderMap({
                center: {
                    lat: 51.502379,
                    lng: -0.103479
                }
            }, 13);
            self.resetMarkers();
        }
        else {
            self.errorAlert("Sorry, I couldn't load the data. :( ");
        }
    };

};
function initialize() { 
    //Initialize the google map
    map = new google.maps.Map(document.getElementById('map'), {
        zoom     : 12,
        center   : {lat: 51.502379, lng: -0.103479},
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    //Initialize the holder for the infowindow
    globals.infoWindow = new google.maps.InfoWindow({ content: "<div class='mdl-spinner mdl-js-spinner is-active'></div>" })
    
    //Apply knocout bindings
    ko.applyBindings(new viewModel());
}









