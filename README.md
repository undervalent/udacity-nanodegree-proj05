# udacity-nanodegree-proj05
The objective of this project was to use the knockout.js framework and Ajax with to create a neighborhood map.

##How to start
Clone or download the udacity-nanodegree-proj05 repo and then inside the build folder, you will find the compressed versions of all files. Open the index.html to view the project. You should see a map that allows you to filter through different attractions in London and to get info from each item from Wikipedia

###Added to latest revision
<ul>
<li>Fixed the async defer issue by adding a callback to the google maps API code</li>
<li>Initialized the map, the kobindings and infowindow placeholder inside of callback function</li>
</ul>
